module.exports = {
    "port" : 80,

    "files": [
        "./src/**/*.{html,htm,css,js}", 
        "./node_modules/**/*"
    ],

    "server": {
            "baseDir": "src",

            "routes": {
                "/node_modules/jquery/dist": "./node_modules/jquery/dist",
                "/node_modules/popper.js/dist": "./node_modules/popper.js/dist",
                "/node_modules/bootstrap/dist": "./node_modules/bootstrap/dist",
                "/node_modules/open-iconic/font": "./node_modules/open-iconic/font",
            }
            
        }
}